import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

export interface LoginForm {
  email: string;
  password: string;
}

export interface RegisterForm {
  name?: string;
  username?: string;
  email?: string;
  password?: string;
  passwordConfirm?: string;
}

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  constructor(private http: HttpClient) {}

  login(loginForm: LoginForm) {
    return this.http
      .post('/api/users/login', {
        email: loginForm.email,
        password: loginForm.password,
      })
      .pipe(
        map((token: { accessToken: string }) => {
          localStorage.setItem('blog-token', token.accessToken);
          return token;
        })
      );
  }

  register(registerForm: RegisterForm) {
    return this.http
      .post('/api/users/', registerForm)
      .pipe(map((user) => user));
  }
}
