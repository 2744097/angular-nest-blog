import {
  Controller,
  Post,
  Body,
  Get,
  Param,
  Delete,
  Put,
  UseGuards,
  Query,
} from '@nestjs/common';
import { UserService } from './user.service';
import { User, UserRole } from './models/user.interface';
import { Observable, of } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { hasRoles } from 'src/auth/decorators/roles.decorator';
import { JwtAuthGuard } from 'src/auth/guards/jwt-guard';
import { RolesGuard } from 'src/auth/guards/roles.guard';
import { Pagination } from 'nestjs-typeorm-paginate';

@Controller('users')
export class UserController {
  constructor(private userService: UserService) {}

  @Post()
  create(@Body() user: User): Observable<User | { error: string }> {
    return this.userService.create(user).pipe(
      map((user: User) => user),
      catchError(err => of({ error: err.mesage })),
    );
  }

  @Get()
  index(
    @Query('page') page = 1,
    @Query('limit') limit = 10,
  ): Observable<Pagination<User>> {
    limit = limit > 100 ? 100 : limit;
    return this.userService.paginate({
      page: Number(page),
      limit: Number(limit),
      route: 'http://localhost:3000/users',
    });
  }

  @Get(':id')
  findOne(@Param() params: { id: string }): Observable<User> {
    return this.userService.findOne(Number(params.id));
  }

  @Delete(':id')
  deleteOne(@Param() params: { id: string }): Observable<any> {
    return this.userService.deleteOne(Number(params.id));
  }

  @Put(':id')
  updateOne(
    @Param() params: { id: string },
    @Body() user: User,
  ): Observable<any> {
    return this.userService.updateOne(Number(params.id), user);
  }

  @hasRoles(UserRole.ADMIN)
  @UseGuards(JwtAuthGuard, RolesGuard)
  @Put(':id/role')
  updateUserRole(
    @Param() params: { id: string },
    @Body() user: User,
  ): Observable<any> {
    return this.userService.updateUserRole(Number(params.id), user);
  }

  @Post('login')
  login(@Body() user: User): Observable<{ accessToken: string }> {
    return this.userService
      .login(user)
      .pipe(map((jwt: string) => ({ accessToken: jwt })));
  }
}
