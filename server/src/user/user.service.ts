import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { UserEntity } from './models/user.entity';
import { Repository } from 'typeorm';
import { User, UserRole } from './models/user.interface';
import { Observable, from, throwError } from 'rxjs';
import { AuthService } from 'src/auth/auth.service';
import { switchMap, map, catchError } from 'rxjs/operators';
import {
  paginate,
  Pagination,
  IPaginationOptions,
} from 'nestjs-typeorm-paginate';

@Injectable()
export class UserService {
  constructor(
    @InjectRepository(UserEntity)
    private readonly userRepository: Repository<UserEntity>,
    private readonly authService: AuthService,
  ) {}

  create(user: User): Observable<User> {
    return this.authService.hashPassword(user.password).pipe(
      switchMap((hashPassword: string) => {
        user.password = hashPassword;
        const newUser = new UserEntity();
        newUser.name = user.name;
        newUser.username = user.username;
        newUser.email = user.email;
        newUser.role = UserRole.USER;
        newUser.password = hashPassword;

        return from(this.userRepository.save(newUser)).pipe(
          map((user: User) => {
            // const { password, ...userWithoutPassword } = user;
            delete user.password;
            return user;
          }),
          catchError(err => throwError(err)),
        );
      }),
    );
  }

  findAll(): Observable<User[]> {
    return from(this.userRepository.find()).pipe(
      map((users: User[]) => {
        users.forEach(user => {
          delete user.password;
        });
        return users;
      }),
    );
  }

  findOne(id: number): Observable<User> {
    return from(this.userRepository.findOne({ id })).pipe(
      map((user: User) => {
        // const { password, ...userWithoutPassword } = user;
        delete user.password;
        return user;
      }),
    );
  }

  deleteOne(id: number): Observable<any> {
    return from(this.userRepository.delete(id));
  }

  updateOne(id: number, user: User): Observable<any> {
    // const { email, password, ...userToUpdate } = user;
    delete user.email;
    delete user.password;
    delete user.role;
    return from(this.userRepository.update(id, user));
  }

  updateUserRole(id: number, user: User): Observable<any> {
    return from(this.userRepository.update(id, user));
  }

  findByEmail(email: string): Observable<User> {
    return from(this.userRepository.findOne({ email }));
  }

  login(user: User): Observable<string> {
    return this.validateUser(user.email, user.password).pipe(
      switchMap((user: User) => {
        if (user) {
          return this.authService
            .generateJWT(user)
            .pipe(map((jwt: string) => jwt));
        } else {
          return 'wrong credantials';
        }
      }),
    );
  }

  validateUser(email: string, password: string): Observable<User> {
    return this.findByEmail(email).pipe(
      switchMap((user: User) =>
        this.authService.comparePasswords(password, user.password).pipe(
          map((match: boolean) => {
            if (match) {
              // const { password, ...userWithoutPassword } = user;
              delete user.password;
              return user;
            } else {
              throw Error;
            }
          }),
        ),
      ),
    );
  }

  paginate(options: IPaginationOptions): Observable<Pagination<User>> {
    return from(paginate<User>(this.userRepository, options)).pipe(
      map((usersPageable: Pagination<User>) => {
        usersPageable.items.forEach(user => {
          delete user.password;
        });
        return usersPageable;
      }),
    );
  }
}
