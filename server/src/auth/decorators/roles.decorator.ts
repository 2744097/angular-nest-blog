import { SetMetadata, CustomDecorator } from '@nestjs/common';

export const hasRoles = (...hasRoles): CustomDecorator<string> =>
  SetMetadata('roles', hasRoles);
